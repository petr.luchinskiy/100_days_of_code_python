import random

rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''
game_item = [rock, paper, scissors]
print("let's play the game, type 0 for rock, 1 for paper, and 2 for scissors")

human_choose = int(input('choose your decision '))
machine_choose = random.randrange(0, 3)

if human_choose >= 3 or human_choose < 0:
    print("You typed an invalid number, you lose!")
elif human_choose == 0 and machine_choose == 2:
    print('machine_choose ', game_item[machine_choose])
    print('your choose  ', game_item[human_choose])
    print("You win!")
elif machine_choose == 0 and human_choose == 2:
    print('machine_choose ', game_item[machine_choose])
    print('your choose ', game_item[human_choose])
    print("You lose")
elif machine_choose > human_choose:
    print('machine_choose ', game_item[machine_choose], )
    print('you lose ', game_item[human_choose])
elif human_choose > machine_choose:
    print('machine_choose ', game_item[machine_choose])
    print("You win!", game_item[human_choose])
else:
    print('equal', game_item[machine_choose])
    print(game_item[human_choose])
