import random

print(f'welcome to the simple password generator')

len_password = int(input('what count of characters do you want? '))
spec_symbol = int(input('what count of specific symbols do you want? '))
num_symbol = int(input('what count of number do you want? '))

letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
           'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
           'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

spec = random.sample(symbols, spec_symbol)
num = random.sample(numbers, num_symbol)

num_letter = len_password - spec_symbol - num_symbol
letter = random.sample(letters, num_letter)

password = letter + spec + num
random.shuffle(password)

result_string = ''.join(password)
print('your generate password is ', result_string)
