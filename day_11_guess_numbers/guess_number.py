import random

print('try to guess number, you can choose easy or hard mode')
mode = input('type 1 to easy or 2 to hard mode')

true_number = random.randint(1, 100)

if mode == '1':
    count_try = 10

elif mode == '2':
    count_try = 5
else:
    exit('wrong mode')

while True:

    if count_try <1:
        exit('your life is ended')
    else:
        print('you have ', count_try, ' lifes')
        guess = int(input('введите число'))
        if guess < true_number:
            print('low than true_number')
            count_try = count_try-1
        elif guess > true_number:
            print('high than true number')
            count_try = count_try-1
        if guess == true_number:
            print('congratulation you win')

