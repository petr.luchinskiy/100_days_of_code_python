import os

print('welcome to the blind auction')

high = 0
winner = ''
bidders = {}

while True:
	name = input('what is your name? ')
	price = int(input("what is your price? $ "))
	ask = input('have another person? yes or not ')
	
	bidders[name] = price
	if ask == 'yes':
		os.system('cls' if os.name == 'nt' else 'clear')
	else:
		break

for i in bidders:
	price = bidders[i]
	if price > high:
		high = price
		winner = i
	else:
		continue
os.system('cls' if os.name == 'nt' else 'clear')
print(f'winner is {winner}, for price - {high}$')
